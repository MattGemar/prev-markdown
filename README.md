# Prévisualisation de Markdown avec React

Un de mes premiers projets avec la bibliothèque React qui permet de prévisualiser du Markdown en HTML

Ce projet à été créé avec [Create React App](https://github.com/facebook/create-react-app).

## Installation

Dans le répertoire du projet vous pouvez lancer l'application avec les scripts:

### `yarn start`

Ou

### `npm start`
