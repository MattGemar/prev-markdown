export const TextBase =
	'# `Prévisualisateur - MattGemar`\n### Bonjour!\n Voici une de mes premières applications React pour transformer du markdown en *HTML* !\n\n### Exemples des tags markdown\n* *italique*\n* **gras**\n* `monospace`\n* ~~barré~~\n* # h1\n* ## h2\n* ### h3\n* #### etc\n[Mon Gitlab](https://gitlab.com/MattGemar)';
