import './App.css';
import marked from 'marked';
import { TextBase } from '../../Assets/TextBase';
import React, { useState } from 'react';

function App() {
	// State
	const [text, setText] = useState(TextBase);

	const changeTextHandler = (event) => {
		const Nouveautext = event.target.value;
		setText(Nouveautext);
	};

	const renderMarkdown = (text) => {
		return { __html: marked(text) };
	};

	return (
		<div className='App'>
			<div className='columns'>
				<div className='column'>
					<textarea value={text} onChange={changeTextHandler} rows='35' />
				</div>
				<div className='column'>
					<div dangerouslySetInnerHTML={renderMarkdown(text)} />
				</div>
			</div>
		</div>
	);
}

export default App;
